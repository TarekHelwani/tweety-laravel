<?php

use App\Http\Controllers\ExploreController;
use App\Http\Controllers\FollowsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TweetLikesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
    Route::post(
        '/tweets',
        [\App\Http\Controllers\TweetController::class, 'store']
    );
    Route::get(
        '/tweets',
        [\App\Http\Controllers\TweetController::class, 'index']
    )->name('home');

    Route::post(
        '/tweets/{tweet}/like',
        [TweetLikesController::class , 'store']
    );

    Route::delete(
        '/tweets/{tweet}/like',
        [TweetLikesController::class , 'destroy']
    );

    Route::post(
        '/profiles/{user:username}/follow',
        [FollowsController::class, 'store']
    )->name('follow');
    Route::get(
        '/profiles/{user:username}/edit',
        [ProfileController::class, 'edit']
    )->middleware('can:edit,user');

    Route::patch(
        '/profiles/{user:username}',
        [ProfileController::class, 'update']
    )->name('profile')
        ->middleware('can:edit,user');

    Route::get(
        '/explore',
        ExploreController::class
    );

});

Route::get('/profiles/{user:username}',
    [ProfileController::class, 'show'])
    ->name('profile');


Auth::routes();

